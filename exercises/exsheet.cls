\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{exsheet}[2013/03/29 LaTeX class]

\ProcessOptions\relax

\LoadClass[a4paper,english]{scrartcl}
\RequirePackage{etex}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}
\RequirePackage{amsmath,amsthm,amssymb,bussproofs,graphicx,xspace}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{agda}
\RequirePackage{newunicodechar}
\RequirePackage{mathtools}
\RequirePackage{framed}
\RequirePackage[protrusion=true,expansion=true]{microtype}
\RequirePackage{multicol}
\RequirePackage{libertine}
\RequirePackage{fontawesome5}

\RequirePackage{geometry}
\geometry{tmargin=2.0cm,bmargin=3.0cm,lmargin=2.8cm,rmargin=2.8cm}

\RequirePackage{hyperref}

\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

\newlength{\titleskip}
\setlength{\titleskip}{1.5em}
\newenvironment{paper}{%
  \pagestyle{plain}%
  University of Verona \hfill
  Ingo Blechschmidt \\
  Minicourse \hfill
  June 2024 \par
  \medskip
}{\newpage}

\newcounter{sheetnumber}
\newenvironment{sheet}[1]{\begin{paper}
  \refstepcounter{sheetnumber}
  \begin{center}
    \Large \textbf{Extracting programs from proofs} \\ \textit{Exercise sheet \thesheetnumber: #1} \\[1em]
  \end{center}
  \vspace{\titleskip}}{\end{paper}}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand*\theenumii{\arabic{enumii}}
\renewcommand{\labelenumi}{(\theenumi)}
\renewcommand{\labelenumii}{\theenumii.}

\newlength{\exerciseskip}
\setlength{\exerciseskip}{1.5em}
\newcounter{aufgabennummer}[sheetnumber]
\renewcommand{\theaufgabennummer}{\thesheetnumber.\arabic{aufgabennummer}}
\newenvironment{exercise}[2][]{
  \refstepcounter{aufgabennummer}
  \textbf{Exercise \theaufgabennummer{}.} \emph{#2} #1 \par
}{\par\vspace{\exerciseskip}}
\newenvironment{exercise*}[2][]{
  \refstepcounter{aufgabennummer}
  \textbf{Exercise* \theaufgabennummer{}.} \emph{#2} #1 \par
}{\vspace{\exerciseskip}}
\newenvironment{exerciseE}[1]{\begin{exercise}{#1}\begin{enumerate}}{\end{enumerate}\end{exercise}}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\newcommand{\C}{\mathcal{C}}

\input{macros}
