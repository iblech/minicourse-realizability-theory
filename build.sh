#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=channel:nixos-unstable -i bash -p "texlive.combine { inherit (texlive) scheme-small pdftex biblatex xstring csquotes libertine bussproofs newunicodechar fontawesome5 xifthen polytable environ ifmtarg stmaryrd lazylist framed; }" -p biber -p "agda.withPackages (p: [ p.standard-library ])" -p perl

set -ex

rm -rf tmp-exercises tmp-solutions public

cp -r exercises tmp-exercises
cp -r exercises tmp-solutions

(
  cd tmp-exercises

  for i in $(find . -name "*.agda" | grep -v "#"); do
    name=$(echo "$i" | cut -c3- | cut -d. -f1 | tr / .)
    [ -e "${i%.agda}.tex" ] && sed -i -e "s/begin{exercise}/begin{exercise}[\\\\flink{$name}]/" ${i%.agda}.tex
  done

  pdflatex main
  biber main
  pdflatex main
  pdflatex main
)

if [ ! -d ~/.agda ]; then
  mkdir -p ~/.agda
  echo standard-library > ~/.agda/defaults
fi

for i in exercises solutions; do
  mkdir -p tmp-$i/Verona2024
  (
    cd tmp-$i

    {
      echo "module Index where"
      echo
      cat main.tex | grep input | cut -d\{ -f2 | cut -d\} -f1 | while read; do
        if [ -e "$REPLY.agda" ]; then
          echo "import $REPLY" | tr / .
        fi
      done
    } > Verona2024/Index.agda

    cp -r Basics Realizability DoubleNegation Verona2024/
    find Verona2024 -name '*.agda' | grep -v "#" | typ="$i" xargs perl -i -pwe '
      BEGIN { $/ = undef }
      s/\bBasics/Verona2024.Basics/g;
      s/\bRealizability/Verona2024.Realizability/g;
      s/\bDoubleNegation/Verona2024.DoubleNegation/g;
      s/\bIndex/Verona2024.Index/g;
      if($ENV{typ} eq "solutions") {
        s/\{--\}//g;
      } else {
        s/\{--\}.*?\{--\}/{!!}/gs;
      }
    '

    agda --html --transliterate Verona2024/Index.agda

    cd html
    ln -s Verona2024.Index.html index.html
  )
done

(
  cd transcripts
  for i in *agda; do
    agda --html "$i"
  done
)

mkdir -p public public/qed
cp index.html poster.pdf qed.html public/
cp qed.html public/qed/index.html
cp topos-horses.jpeg public/qed/
cp tmp-exercises/main.pdf public/exercises.pdf
cp -r tmp-exercises/html public/exercises
cp -r tmp-solutions/html public/solutions
cp -r transcripts/html public/transcripts
