[00:00] Lecture I: Constructive mathematics
        ───────────────────────────────────

[02:12] Thm.: There are irrational numbers x and y such that x^y is rational.

[03:25] Proof: Either √2^√2 is rational or not.

[03:47]        In the first case, we are done by x ≔ √2 and y ≔ √2.

[04:14]        In the second case, we are done by x ≔ √2^√2 and y ≔ √2.
               (Then x^y = √2^(√2·√2) = √2^2 = 2 ∈ ℚ.)

[06:04] This proof is *nonconstructive*. It doesn't actually present a
        (guaranteed to be correct) example for such a pair of irrational numbers.

[07:15] Proof: Set x ≔ √2 and y ≔ log_{√2} 3. (Then x^y = 3 ∈ ℚ.)

[08:00] This alternative proof is constructive. It explicitly presents a
        witness of the claim.

[09:00] Thm.: Given any natural number n ∈ ℕ, there is a prime p > n.

[09:30] Proof (constructive): Any prime factor of n! + 1 will do.

[12:30] Thm.: Every function α : ℕ → ℕ is good in the sense that for some i < j,
              α(i) ≤ α(j).

[13:30] Proof (unconstructive): There is a minimal value α(i). Set j ≔ i + 1.

[18:40] "Def.": Constructive mathematics
                  = a flavor of mathematics centered around informative constructions
[19:50]           = the same as classical mathematics, but without LEM, DNE and AC
[21:40]           = mathematics built on intuitionistic logic instead of classical logic

[23:00] LEM: φ ∨ ¬φ   (for any formula φ)
[24:50] DNE: ¬¬φ ⇒ φ  (for any formula φ)
[26:00] AC:  axiom of choice

[28:00] Note: This characterization of constructive mathematics is misleading!
              As we will learn in Lecture 3, constructive mathematics is actually an
              expansion of classical mathematics, not a restriction.

[30:10] We need to distinguish the following two figures of proof:

[30:40] 1. Claim: φ.
           Proof: Assume for the sake of contradiction that ¬φ.
                  Then ..., ↯.
                  Hence φ.

[32:13]    This is a proper proof by contradiction. Constructively, such an argument
           only shows ¬¬φ. We would need DNE or LEM to conclude φ from ¬¬φ.

[32:30] 2. Claim: ¬ψ.
           Proof: Assume ψ.
                  Then ..., ↯.
                  Hence ¬ψ.

[33:30]    This is a constructively acceptable proof of a negated statement.
[34:10]    (By definition, ¬ψ :≡ (ψ ⇒ ↯).)

[35:00] Thm.: √2 is not rational.

        Proof (constructive): Assume that √2 is rational. Then √2 = a/b, ..., ↯.

[39:50] In constructive mathematics, we use the same logical symbols as in classical
        mathematics, but we interpret them differently (BHK interpretation):

[40:50] statement   classical meaning             constructive meaning
        ─────────   ─────────────────             ──────────────────────────
[41:30] φ           φ is true.                    We have a witness for φ.
[42:40] α ∧ β       α and β are both true.        We have a witness for α and one for β.
[43:30] α ∨ β       α is true or β is true.       We have a witness for α, or we have one for β.
[45:00] α ⇒ β       If α is true, then so is β.   We have a uniform procedure for turning witnesses
                                                  for α into witnesses for β.
[47:10] ¬α          α is false.                   There is no witness for α.
[47:45] ∀x:X. φ(x)  For all x : X, φ(x) is true.  We have a uniform procedure which inputs
                                                  an arbitrary x : X and outputs a witness for φ(x).
[50:20] ∃x:X. φ(x)  For at least one x : X,       We have an x : X and a witness for φ(x).
                    φ(x) is true.

[51:45] Ex.: ¬(α ∧ β) ⇒ ((¬α) ∨ (¬β)).
[53:55]      unconstructive!

[55:10] Ex.: ¬¬φ ⇒ φ.
             unconstructive

[56:50] Ex.: φ ⇒ ¬¬φ.
[57:30]      constructive

[58:00] Ex.: ¬¬∃x. the key is at position x   vs.   ∃x. the key is at position x.

[60:30] Constructive mathematics supports finer distinctions than classical mathematics.

[64:00] What more do we know if we have proved a theorem by restricted
        means than if we merely know the theorem is true? ─Georg Kreisel

        What are uses of constructive mathematics?

[66:00] - fun
[66:55] - philosophy
[67:10] - mental hygiene
[68:15] - better appreciation for classical logic
[69:20] - elegance assistance
[72:50] - program extraction
[74:45] - automatic parameter-dependence
[78:00] - axiomatic freedom, internal language of toposes
[81:50] - bounds, growth rates, ...

[86:00] Prop.: 1. Every inhabited detachable set of natural numbers contains a minimum.
[87:00]        2. Every inhabited set of natural numbers does *not not* contain a minimum.
[87:30]        3. If every inhabited set of natural numbers contains a minimum, then LEM.

[89:10] Def.: A set X is *inhabited* if and only if ∃x:X.
[89:20]       A set X is *nonempty*  if and only if ¬(X = ∅), i.e. if and only if ¬¬∃x:X.

[92:00] Def.: A set X ⊆ ℕ is *detachable* if and only if ∀n:ℕ. n ∈ X ∨ ¬(n ∈ X).
