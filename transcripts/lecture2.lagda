Claim: φ ⇒ ¬ ¬ φ

Proof: Assume φ, we need to show (¬ φ) ⇒ ⊥.
       So assume φ ⇒ ⊥; we need to show ⊥.
       By combining the asumptions φ and φ ⇒ ⊥,
       we obtain ⊥.

Grundlagenstreit


Realizability
─────────────

"HA ⊢ φ" is an abbreviation for: There is an HA-proof of the sequent "⊤ ⊢ φ".

Soundness Theorem: If HA ⊢ φ, then ⊩ φ.
                      ^^^^^^       ^^^
                   "HA proves φ"   "φ is realized"
                  Heyting          "there is a program witnessing φ"
                 arithmetic

Example: HA proves that beyond every number n, there is a prime p>n.
         Hence: There is a program which reads a number n as input
                and outputs a prime p > n.

Uses: - integrated developments
      - metatheory, for instance to prove:
           If HA ⊢ (α ∨ β), then (HA ⊢ α) or (HA ⊢ β).
           Totally false for classical systems like PA or ZFC.
      - philosophy
      - recognizing/understanding proofs

Def.: ⊩ φ if and only if there exists e ∈ ℕ such that e ⊩ φ.
      (We picture e to denote the e'th Turing         ^^^^^^
      machine in some adequate enumeration         "e realizes φ"
      of all Turing machines.)

Def.: e ⊩ (φ ⇒ ψ) iff for every r ∈ ℕ such that r ⊩ φ,
                       e·r↓ and e·r ⊩ ψ.
                       ^^^^     (e·r is the result of applying
                the computation    the input r to the e'th
                e·r terminates     Turing machine)
                with a
                well-formed
                natural number

      e ⊩ ⊥ iff contradiction.

      e ⊩ ⊤ iff true.

      e ⊩ x = y iff ⟦x⟧ = ⟦y⟧.

      e ⊩ (∀x:ℕ. φ(x)) iff for every x ∈ ℕ, e·x↓ and e·x ⊩ φ(x).

      e ⊩ (∃x:ℕ. φ(x)) iff π₁ · e ↓ and π₂ · e ↓ and
                         π₂ · e ⊩ φ(π₁·e)

      e ⊩ (φ ∨ ψ) iff π₁ · e ↓ and π₂ · e ↓ and
                       (if π₁·e = 0, then π₂·e ⊩ φ) and
                       (if π₁·e ≠ 0, then π₂·e ⊩ ψ).

      e ⊩ (φ ∧ ψ) iff π₁ · e ↓ and π₂ · e ↓ and
                       π₁ · e ⊩ φ and π₂ · e ⊩ ψ.
             (where πᵢ is the Turing machine which extracts
              the i'th component from a pair)

Ex.: e ⊩ ¬φ  iff  e ⊩ (φ ⇒ ⊥)  iff for every r ∈ ℕ such that r ⊩ φ, 
                                        e·r↓ and e·r ⊩ ⊥
                                iff for every r ∈ ℕ such that r ⊩ φ, contradiction
                                iff there is no r ∈ ℕ such that r ⊩ φ
                                iff ¬ (⊩ φ).

     Hence: If ¬φ is realized at all, then it is so by any number.

Ex.: e ⊩ ¬¬ψ  iff  ¬ (⊩ ¬ψ)   iff ¬ (∃r∈ℕ. r ⊩ ¬ψ)
                               iff ¬ (∃r∈ℕ. ¬ (⊩ ψ))
                               iff ¬¬ (⊩ ψ).

Ex.: For most ψ, we don't have ⊩ (¬¬ψ ⇒ ψ).

HA (Heyting Arithmetic) is a foundational system for working with natural numbers.
It's the intuitionistic cousin of PA (Peano Arithmetic).

The language of HA is: 0, S, +, *.
The axioms of HA:
1. ∀x. x + zero = x
2. ∀x. ∀y. x + S(y) = S(x + y)
3. ...
4. induction scheme
The rules of HA are: (see exercise sheet)

Theorem: If HA ⊢ φ, then φ is true.
Proof: By inspection, the axioms of HA are all true; and the rules of HA
       preserve truth.

Proof of soundness: By inspection, the axioms of HA are all realized;
                    and the rules of HA preserve realizability.

                    One part of this: (orally)

                    For instance, let's consider the rule

                        ───────────────
                           φ ∧ ψ ⊢ φ

                    So check that ⊩ (φ ∧ ψ ⇒ φ).
                    So we need to find a natural number e such that
                    for every r ∈ ℕ with r ⊩ (φ ∧ ψ), e·r↓ and e·r ⊩ φ.
                    This is done by setting e := π₁.

                    Now let's consider the rule
                        ───────
                         ⊥ ⊢ φ
                    We need to find a realizer e ∈ ℕ such that e ⊩ (⊥ ⇒ φ).
                    Hence: e needs to satisfy: for every r∈ℕ s.th. r⊩⊥,
                    e·r↓ and e·r ⊩ φ. We can use e ≔ 42.

                    Now let's consider the rule
                         φ ⊢ χ      ψ ⊢ χ
                        ──────────────────
                            φ ∨ ψ ⊢ χ
                    Given e and f such that e ⊩ (φ ⇒ χ) and f ⊩ (ψ ⇒ χ),
                    we need to construct a number g ∈ ℕ such that
                    g ⊩ ((φ ∨ ψ) ⇒ χ).

                    Now let's consider the axiom
                      (P(0) ∧ (∀n:ℕ. P(n) ⇒ P(S(n)))) ⇒ (∀n:N. P(n)).
                    A realizer e ∈ ℕ for this axiom is a machine which...

                    base : P 0
                    step : (n : ℕ) → P n → P (S n)

                    step 3 (step 2 (step 1 (step 0 base))) : P 4


In classical logic, we neither have

    φ   ⇒   ⊩ φ

nor

    φ   ⇐   ⊩ φ.

(Only for some statements φ.)

(Note: A statement φ is true in the effective topos
if and only if φ is realized.)

In effective topos, it is true that

    φ   ↔    ⊩ φ.

In other words, we have the following insight on realizability:

   ⊩ (φ ↔ (⊩ φ)).        "Idempotence"
