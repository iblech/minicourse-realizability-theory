- synthetic differential geometry
- synthetic algebraic geometry
- synthetic homotopy theory (better known as HoTT)
- synthetic computability theory

- synthetic Euclidean geometry

  Euclid: postulated notion of points, notion of lines,
          axioms like "every two distinct points are connected
          by a line", ...

  Euclid did NOT talk about:
  ℝ, sin : ℝ → ℝ, ℝ², π
  -- this would be the "analytic approach"

  0 := {}
  1 := { 0 } = { {} }
  2 := { 0, 1 } = { {}, {{}} }
  3 := ...
  In this way, create ℕ.
  From ℕ, create ℤ.
  From ℤ, create ℚ.
  From ℚ, create ℝ.
  From ℝ, create ℝ².
  ...


In ordinary (analytic) computability theory, we have theorems
like "There is a Turing machine which checks whether a given
number is prime or not". This theorem refers to Turing machines
and the proof requires explicit constructions of Turing machines.

In synthetic computability theory, we express the same theorem
more simply as follows: "Every number is prime or not."
Observe: No mention of Turing machines.

The connection is as follows: The theorem above holds if and only
if the theorem below holds in the effective topos.

Synthetic computability theory = work internally to the effective
topos.


In ordinary computability theory, we have the following theorem:
The set of all total computable functions ℕ → ℕ is not recursively
enumerable. In other words, there is no Turing machine M which
reads a number n as input and outputs a Turing machine computing
the n'th total computable function ℕ → ℕ.

In synthetic computability theory, this theorem can be expressed
as follows: The set of all total functions ℕ → ℕ is not countable.
In other words, there is no surjective function M : ℕ → ℕ^ℕ.



In ordinary computability theory, we have the following theorem:
There is no halting oracle. In other words: There is no Turing
machine H which reads a Turing machine M as input and outputs
0 or 1 depending on whether M terminates or not.

In synthetic computability theory, this theorem can be expressed
as follows:

    ¬ (∀M : ℕ ⇀ ℕ. ∀x : ℕ. Mx↓ ∨ Mx↑).

However, we cannot give a constructive proof of this theorem
(as there isn't even a classical proof); in order to give a proof,
we need to postulate an anti-classical axiom:

    EPF: ∃Θ : ℕ ↠ (the set of all partial functions ℕ ⇀ ℕ).

    So for every partial function f : ℕ ⇀ ℕ
    there is a number c ∈ ℕ such that Θ(c) = f.

Note: In intuitionistic logic, we can prove LEM ∧ EPF ⇒ ⊥.
Hence EPF does NOT hold in classical mathematics.
However classical mathematics agrees that EPF is realized.

Proof:

    Assume for the sake of a contradiction that
        ∀M : ℕ ⇀ ℕ. ∀x : ℕ. Mx↓ ∨ Mx↑.

    Then in particular
        ∀x : ℕ. Θ(x)(x)↓ ∨ Θ(x)(x)↑.

    So we can define a partial function f : ℕ → ℕ by

            { 0,          if Θ(x)(x)↑
        x ↦ { undefined,  if Θ(x)(x)↓

    By EPF, there is a number c ∈ ℕ such that Θ(c) = f.
    We observe a contradiction as follows:

        f(c)↓ ⇔ f(c) = 0 ⇔ Θ(c)(c)↑ ⇔ f(c)↑




The following definition

        { -1,  if x < 0
    x ↦ {  0,  if x = 0
        {  1,  if x > 0

defines, in constructive mathematics, a PARTIAL function ℝ ⇀ ℝ.
Its domain (the set of numbers on which it is defined) is

    { x : ℝ | x < 0 ∨ x = 0 ∨ x > 0 } ⊆ ℝ.
